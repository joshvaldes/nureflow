#include <PID_v1.h>

#include <SPI.h>
#include <SoftwareSerial.h>
#include <Adafruit_MAX31855.h>

/////////////////// PID

double Kp = 2, Ki = 5, Kd = 1;
double setTemp, readTemp, heaterPower;
PID myPID(&readTemp, &heaterPower, &setTemp,Kp,Ki,Kd, DIRECT); // DIRECT - when you want the temp to go up you should turn up the heat


unsigned long timer;



String heatSchedule = "leaded"; //what program to run by default
String phase = "on"; //what phase the heating program is in: on, precool, begin, warmup, reflow, cooldown
//temperature settings for leaded and unleaded warm up complete temp all in C
int leadedWarmupTemp = 150;
int unleadedWarmupTemp = 200;
int leadedReflowTemp = 183;
int unleadedReflowTemp = 217;
int undershootReflowBy = 0; //number of degrees C to declare reflow temp at
int reflowDuration = 200000; //milliseconds  100 seconds is 100,000ms
int loadBoardTemp = 50; //minimum temp to wait for cooling down before loading a new board


boolean heating = false; //true if the heater is turned on
bool lastHeat;

bool warmReached = false;
int warmTemp = 140;
bool peakReached = false;
int peakTemp = 183; //hold 183C for 100s
bool peakDone = false;
unsigned long peakTime;
bool timerTicked = false;

boolean reflowReached = false; // true if reflow temp has been reached
boolean startup = true;

unsigned long startTime; //time the latest heating program phase started
unsigned long startTemp; //temp when the program started
unsigned long reflowStartTime; //time reflow temp was reached

//places to keep track of the most recent time and temp readings
unsigned long nowTemp;
unsigned long nowTime;

int buttonA = 11; //button connects 2 digital pins
int buttonB = 10;
int ledButton = 13;

int tempReading;
int desiredTemp = 50;
int preheatTemp = 100;

int relayPin = 2;
int lcdTX = 6;

int thermoCLK = 5;
int thermoCS = 4;
int thermoDO = 3;


// Initialize the Thermocouple
Adafruit_MAX31855 thermocouple(thermoCLK, thermoCS, thermoDO);

SoftwareSerial lcd(7, lcdTX); // RX, TX

void setup(){
  lcd.begin(9600);
  

  pinMode(relayPin, OUTPUT); 
  pinMode(ledButton, OUTPUT); 

  myPID.SetMode(AUTOMATIC);
  
   
}


void lcdZero(){
  lcd.write(0xFE);
  lcd.write(128);    // 0,0 position
  delay(10);
}

void lcdLineTwo(){
  lcd.write(0xFE);
  lcd.write(192); 
  delay(10);
}


/*
void updateHeater(){
//    checkPhase();
    heatTo(programmedTemp()); //heat to some temp
}
*/


void showTemp(){
 
  lcdZero();
//  lcd.print("temp = ");
  lcd.print(int(thermocouple.readCelsius()));
  lcd.print("c");
  delay(10);
}

void clearLCD(){
   lcd.write(0xFE);   //command flag
   lcd.write(0x01);   //clear command.
   delay(10);
}




void updateDisplay(){
 
  lcdZero();
//  lcd.print("temp = ");
  lcd.print(int(thermocouple.readCelsius()));
  lcd.print("C");
  lcd.print("  ");
  lcd.print(phase);
  lcdLineTwo();
  if (heating == true) lcd.print("heating");
  if (heating == false) lcd.print("cooling");
  delay(10);

}

void heatTo(int tempToBe){

 
//warm up max3 degrees per second to - 150C for lead, 200C lead free
//peak at 183C lead, and 217C lead free for - 100 seconds
//cool down slower than 6C per second


tempReading = thermocouple.readCelsius();
  
  if (tempReading < tempToBe){
    digitalWrite(relayPin, HIGH); 
    heating = true;
    delay(50);
 
  }else if (tempReading >= tempToBe){
    digitalWrite(relayPin, LOW);
    heating = false;
    delay(50);
  }
    
}// end heatTo()

void heatCheck(int desiredTemp){ //35c is just above 90f
  readTemp = thermocouple.readCelsius();
  setTemp = desiredTemp;

  myPID.Compute();
  heatTo(desiredTemp);
  
  clearLCD();
  lcdZero();
  lcd.print("pow ");
  lcd.print(int(heaterPower));
  lcdLineTwo();
  lcd.print("t ");
  lcd.print(int(readTemp));
  lcd.print(" ");
  if(heating == true) lcd.print("H");
  if(heating == false) lcd.print("C");

  //analogWrite(3,Output);


}//end heat check

void loopHeat(){

    
    if(heaterPower < 10 && heating == true){
      //digitalWrite(relayPin, LOW);   
      heating = false;
    }
    if(heaterPower > 10){
      //digitalWrite(relayPin, HIGH);
      heating = true;
    }
}

void timerCheck(){
  timer = millis();

  if(readTemp > 140) warmReached = true;
  if(readTemp > peakTemp) peakReached = true;
  if(peakReached == true && timerTicked == false){
    peakTime = timer;
    timerTicked = true;
  }
  
  if((timer - peakTime) > 180000) peakDone = true;
}

void loop(){

//warm up max3 degrees per second to - 150C for lead, 200C lead free
//peak at 183C lead, and 217C lead free for - 100 seconds
//cool down slower than 6C per second


int tempTarget;

if(warmReached == false && peakDone == false) tempTarget = 140;
if(peakReached == false && peakDone == false) tempTarget = peakTemp;
if(peakReached == true && peakDone == false) tempTarget = peakTemp;
if(peakReached == true && peakDone == true) tempTarget = 100;

timerCheck();
heatCheck(tempTarget);
loopHeat();



if(lastHeat != heating){
  if(heating == true) digitalWrite(relayPin, HIGH);
  if(heating == false) digitalWrite(relayPin, LOW);
  lastHeat = heating;
}





 
}//end loop()


/*

int programmedTemp(){
  int t;

  nowTemp = thermocouple.readCelsius();
  nowTime = millis();

  if (heatSchedule == "leaded" && nowTemp < leadedWarmupTemp && phase == "warmup"){
      t = startTemp + (((nowTime - startTime) / 1000) * 3);
   
  }else if (heatSchedule == "unleaded" && nowTemp < unleadedWarmupTemp && phase == "warmup"){
      t = startTemp + (((nowTime - startTime) / 1000) * 3);
      
  }else if (heatSchedule == "leaded" && nowTemp < leadedReflowTemp && phase == "reflow"){
      t = startTemp + (((nowTime - startTime) / 1000) * 3);
      
  }else if (heatSchedule == "unleaded" && nowTemp < unleadedReflowTemp && phase == "reflow"){
      t = startTemp + (((nowTime - startTime) / 1000) * 3);

  }else if (phase == "cooldown"){
      t = startTemp - (((nowTime - startTime) / 1000) * 6);
      
  }else if (phase == "precool"){
      t = loadBoardTemp;
  }
  
  return t;
}//end programmedTemp()



void checkPhase(){

    //check the temp, and if it's high enough, switch to reflow phase  
  nowTemp = thermocouple.readCelsius(); 
  nowTime = millis();

  //check to see if oven is cool enough to start a cycle
  if (phase == "on"){
    if (nowTemp > loadBoardTemp){
      startTemp = thermocouple.readCelsius();
      startTime = millis();
      phase = "precool";
    }
    
    if (nowTemp < loadBoardTemp){
      phase = "ready";
    }
  }
  if (phase == "precool" && nowTemp < loadBoardTemp) phase = "ready";
  
  
  //cooled enough to start heating up?
  if (phase == "ready"){
    phase = "warmup";
    startTemp = thermocouple.readCelsius();
    startTime = millis();
  }
  //time for reflow?
  if (phase == "warmup" && heatSchedule == "leaded" && nowTemp >= leadedWarmupTemp){
    phase = "reflow";
    startTemp = thermocouple.readCelsius();
    startTime = millis();
  }else if (phase == "warmup" && heatSchedule == "unleaded" && nowTemp >= unleadedWarmupTemp){
    if (phase == "warmup" && heatSchedule == "leaded" && nowTemp >= leadedWarmupTemp){
    phase = "reflow";
    startTemp = thermocouple.readCelsius();
    startTime = millis();
    }
  }

   //check for start of reflow
   if (reflowReached == false && nowTemp >= (leadedReflowTemp - undershootReflowBy) && heatSchedule == "leaded"){
     reflowStartTime = millis();
     reflowReached = true;

   }else if (reflowReached == false && nowTemp >= (unleadedReflowTemp - undershootReflowBy) && heatSchedule == "unleaded"){
     reflowStartTime = millis();
     reflowReached = true;
  
   }
   
   //check for cooldown time
   if (reflowReached == true && nowTime > (reflowStartTime + reflowDuration)){
     phase = "cooldown";
     startTemp = thermocouple.readCelsius();
     startTime = millis();
   }else if (reflowReached == true && nowTime <= (reflowStartTime + reflowDuration)){
    
   }
}//end check phase, having set phase = on, precool, begin, warmup, reflow, cooldown

*/

